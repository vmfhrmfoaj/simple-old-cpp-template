#!/bin/bash

CUR_DIR=$(pwd)
GTEST_DIR=gtest
GTEST_VER=1.8.0
FILE=release-$GTEST_VER.zip

rm -rf $GTEST_DIR
wget https://github.com/google/googletest/archive/$FILE
if [ ! -e $FILE ]; then
    echo "Could not download GoogleTest."
    exit 1
fi
unzip  $FILE
rm -rf $FILE
mv googletest-release-$GTEST_VER $GTEST_DIR
if [ ! -d gtest ]; then
    echo "Could not download GoogleTest successfully."
    exit 1
fi

cd $CUR_DIR/$GTEST_DIR/googletest
autoreconf -fvi
./configure
make

cd $CUR_DIR/$GTEST_DIR/googlemock
autoreconf -fvi
./configure
make

exit 0
