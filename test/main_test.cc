#include "gtest/gtest.h"
#include "gmock/gmock.h"

class Foo {
 public:
  virtual void doSometing() = 0;
};

class MockFoo : public Foo {
 public:
  MOCK_METHOD0(doSometing, void());
};

TEST(FixMe, equal) {
  EXPECT_EQ(1, 2);
}

TEST(FixMe, mock) {
  MockFoo foo;
  EXPECT_CALL(foo, doSometing());
  // foo.doSometing();
}
